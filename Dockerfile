ARG ARTIFACT_NAME=assignment-0.0.1-SNAPSHOT.jar
ARG PORT=8010
ARG PROFILE=h2
FROM openjdk:8-alpine

ARG ARTIFACT_NAME
ARG PORT
ARG PROFILE

RUN mkdir /home/app
COPY /target/${ARTIFACT_NAME} /home/app/service.jar

ENTRYPOINT ["java", "-jar", "-Dserver.port=${PORT}","-Dspring.profiles.active=${PROFILE}", "/home/app/service.jar"]





